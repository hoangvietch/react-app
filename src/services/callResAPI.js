import axios from 'axios';
axios.defaults.baseURL = 'http://localhost:8082/api/v1';

export function loginForm(method, url, options){
    return axios({
        method,
        url,
        options
    })
}