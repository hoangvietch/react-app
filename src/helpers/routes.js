import Home from './../components/home/Home';
import Login from '../components/auth/Login';

export const routes = [
    {
    path: '/dashboard',
    component: Home
  }, 
  {
    path: '/login',
    component: Login
  }
];