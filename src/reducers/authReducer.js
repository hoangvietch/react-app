import * as actions from './../constants/actionTypes';
// Create defaultState **
const defaultState = {
    currentUser: {},
    error: {}
};

// Create reducer **
export default function authReducer(state = defaultState, action){
    switch (action.type) {
        case actions.AUTHETICATION_LOGIN:
        return {...state, currentUser: action.user}
        case actions.FAILED_LOGIN:
            return {...state, error: action.msg}
        default:
            return state;
    }
}
