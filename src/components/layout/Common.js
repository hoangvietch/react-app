import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {Button} from 'antd';
import { Link } from "react-router-dom";
import Sidebar from './Sidebar';
import Footer from './Footer';
class Common extends Component {
    constructor(props){
        super(props)
      
    this.logOutUser = this.logOutUser.bind(this)
     
    }
    logOutUser(){
        localStorage.removeItem('user');
        console.log(this.props.history.location.pathname)
        this.props.history.push('/login')
    }
    render() {
       
        return (
            <div>
                <Sidebar />
                <div className="content-wrapper" style={{ minHeight: '100vh' }}>
                    {/* Content Header (Page header) */}
                    <div className="content-header">
                        <div className="container-fluid">
                            <div className="row mb-2">
                                <div className="col-sm-6">
                                </div>{/* /.col */}
                                <div className="col-sm-6">
                                    <ol className="breadcrumb float-sm-right">
                                        <li className="breadcrumb-item"><Link to='/dashboard'>Home</Link></li>
                                        <li className="breadcrumb-item"><Button onClick={this.logOutUser }>Logout</Button></li>
                                    </ol>
                                </div>{/* /.col */}
                            </div>{/* /.row */}
                        </div>{/* /.container-fluid */}
                    </div>
                    {/* /.content-header */}
                    {/* Main content */}
                    <div className="content">
                        <div className="container-fluid">
                            <div className="row">
                                {this.props.children}
                            </div>
                            {/* /.row */}
                        </div>
                        {/* /.container-fluid */}
                    </div>
                    {/* /.content */}
                </div>
                <Footer />
            </div>

        )
    }
}

export default withRouter(Common);