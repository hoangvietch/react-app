import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import Common from './../layout/Common';
import ChartComponent from './Chart';
class Home extends Component {
  render() {
    return (
      <Common>
        {
          <div className="col-lg-6">

            <ChartComponent />
          </div>

        }
      </Common>

    );
  }
}
export default withRouter(Home);