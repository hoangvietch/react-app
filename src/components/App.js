import React, { Component } from "react";
import { routes } from './../helpers/routes';
import { BrowserRouter , Switch, Route } from "react-router-dom";

class App extends Component {
    constructor(props){
        super(props)
        this.state = {
            user : localStorage.getItem('userCurrent')
        }
    }

    render() {
        return (
            <div className="App">
                <BrowserRouter>
                    <Switch>
                       { routes.map((route, key) => <Route key={key} path={route.path} component={route.component} />) }
                       <Route from to='/dashboard' />
                    </Switch>
                </BrowserRouter>

            </div>
        );
    }
}

export default App;