import React, { Component } from 'react';
import { Alert } from 'antd';
import { withRouter } from 'react-router-dom';
import styles from './styles';
import { withStyles } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { connect } from 'react-redux';
import { getUser } from '../../actions/authAction';
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: this.props.currentUser
        }
        this.handleSubmitForm = this.handleSubmitForm.bind(this)
    }
    handleSubmitForm(e) {
        e.preventDefault();
        const email = document.getElementById('email').value;
        const password = document.getElementById('password').value;
        let data = {
            email, password
        }
        this.props.onhanldeSubmit(data);

    }

    static getDerivedStateFromProps(nextProps, prevState) {

        if(nextProps.currentUser !== prevState.currentUser){
            localStorage.setItem('user', nextProps.currentUser)
            nextProps.history.push('/dashboard')
            return {
                currentUser: nextProps.currentUser
            }
        }
        return null
        
    }

    render() {
        const { classes, error } = this.props;
        return (
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>

                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Login Your Panel
                </Typography>
                    {Object.keys(error).length > 0 && <Alert style={{ marginTop: 20, marginBottom: 10 }} message={Object.values(error)} type="error" showIcon />}
                    <form onSubmit={this.handleSubmitForm} className={classes.form}>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                            autoFocus
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                        />
                        <FormControlLabel
                            control={<Checkbox value="remember" color="primary" />}
                            label="Remember me"
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            Sign In
                  </Button>
                    </form>
                </div>
                <Box mt={8}>
                </Box>
            </Container>
        );
    }
}
const mapStateToProps = state => {
    return {
        currentUser: state.authReducer.currentUser,
        error: state.authReducer.error
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onhanldeSubmit: (data) => dispatch(getUser(data))
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Login)));