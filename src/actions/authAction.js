import axios from 'axios';
axios.defaults.baseURL = 'http://localhost:8082/api/v1';

export const getUser = (data) => async dispatch => {
    await axios.post('/auth/login', data)
    .then(res => {
        const user = res.data.token
        dispatch({ type: 'AUTHETICATION_LOGIN', user});
    })
    .catch(err => {
        const msg = err.response.data.message
        dispatch({type: 'FAILED_LOGIN',msg})
    });
    
    
};